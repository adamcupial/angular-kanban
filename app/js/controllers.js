'use strict';

/* Controllers */

angular.module('myApp.controllers', ['LocalStorageModule'])
  .config(['localStorageServiceProvider', function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('miniKanban');
    // localStorageServiceProvider.setStorageCookieDomain('example.com');
    // localStorageServiceProvider.setStorageType('sessionStorage');
  }])
  .controller('MainViewCtrl', [
    '$scope',
    'localStorageService',
    function($scope, localStorageService) {

    var columns = [
      {
        type: 'icebox',
        title: 'Icebox'
      },
      {
        type: 'todo',
        title: 'TODO'
      }
    ];

    $scope.columns = columns;
    $scope.items = localStorageService.get('items') || [];
    $scope.newItem = {title: '',description: '', id: null, column: null};

    $scope.formVisible = false;
    $scope.formEdit = false;

    $scope.addItem = function () {
      var newItem = {
        title: $scope.newItem.title,
        description: $scope.newItem.description,
        column: $scope.newItem.column || columns[0].type,
        expanded: false,
        id: $scope.newItem.id
      };
      if (newItem.id != null) {
        var itemToChange = $scope.items.filter(function (it) { return it.id === newItem.id; })[0];
        var itemIndex = $scope.items.indexOf(itemToChange);
        $scope.items.splice(itemIndex, 1, newItem);
      } else {
        newItem.id = new Date().getTime();
        $scope.items.push(newItem);
      }
      localStorageService.set('items', $scope.items);
      $scope.formVisible = false;
      $scope.formEdit = false;
      $scope.newItem = {title: '', description: ''};
    };

    $scope.editItem = function (item) {
      $scope.newItem.title = item.title;
      $scope.newItem.description = item.description;
      $scope.newItem.column = item.column;
      $scope.newItem.id = item.id;
      $scope.formEdit = true;
      $scope.formVisible = true;
    };
    
  }]);

